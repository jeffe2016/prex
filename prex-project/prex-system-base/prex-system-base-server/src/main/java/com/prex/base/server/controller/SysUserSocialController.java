package com.prex.base.server.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Classname SysSocialController
 * @Description 社交登录
 * @Author Created by Lihaodong (alias:小东啊) lihaodongmail@163.com
 * @Date 2019-07-17 16:51
 * @Version 1.0
 */
@RestController
@RequestMapping("/social")
public class SysUserSocialController {

}
